#include "stdafx.h"
#include <iostream>
#include <vector>
#include <fstream>

const int MEM_SIZE = 10;
int MEM[MEM_SIZE];
std::vector<char> instructs;
unsigned int curPtr = 0;
unsigned int instPtr = 0;

unsigned int findMatchingBrace(std::vector<char>& _insts, bool _open, unsigned int _strtPos)
{
	unsigned int depth = 0;
	unsigned int curPos = _strtPos;
	if (_open)
	{
		++curPos;
		while (true)
		{
			if (curPos >= _insts.size())
			{
				std::cout << "[CRITICAL ERROR]: Mismatched brackets! (Missing close bracket)..." << std::endl;
			}
			if (_insts[curPos] == ']' && depth == 0)
				return curPos;
			else if (_insts[curPos] == ']' && depth > 0)
			{
				--depth;
				++curPos;
			}
			else
			{
				if (_insts[curPos] == '[')
					++depth;
				++curPos;
			}
		}
	}
	else
	{
		--curPos;
		while (true)
		{
			if (_insts[curPos] == '[' && depth == 0)
				return curPos-1;
			else if (_insts[curPos] == '[' && depth > 0)
			{
				--depth;

				if (curPos == 0)
				{
					std::cout << "[CRITICAL ERROR]: Mismatched brackets! (Missing open bracket)..." << std::endl;
				}
				--curPos;
			}
			else
			{
				if (_insts[curPos] == ']')
					++depth;

				if (curPos == 0)
				{
					std::cout << "[CRITICAL ERROR]: Mismatched brackets! (Missing open bracket)..." << std::endl;
				}
				--curPos;
			}
		}
	}
}

void print_mem()
{
	for (unsigned int cnt = 0; cnt < MEM_SIZE; ++cnt)
	{
		std::cout << cnt << ": " << MEM[cnt] << std::endl;
	}
}

int main()
{
	for (unsigned int cnt = 0; cnt < MEM_SIZE; ++cnt)
	{
		MEM[cnt] = 0;
	}

	std::ifstream inptFile("challenge.bf");
	char inpt;
	if (inptFile.is_open())
	{
		while (inptFile.get(inpt))
		{
			if (inpt == '+' || inpt == '-' || inpt == '<' || inpt == '>'
				|| inpt == '[' || inpt == ']' || inpt == '.' || inpt == ','
				|| inpt == '!')
			{
				instructs.push_back(inpt);
			}
			else
			{
				//Discard it
			}
		}
	}


	for (unsigned int cnt = 0; cnt < instructs.size(); ++cnt)
	{
		//std::cout << cnt << ": " << instructs[cnt] << std::endl;
	}

	std::cout << "Starting Brainfuck interpreter ..." << std::endl;

	while (true)
	{
		//std::cout << "Instruction: " << instructs[instPtr] << " CurPtr: " << curPtr << " InstPtr: " << instPtr << std::endl;
		//print_mem();
		
		if (instPtr >= instructs.size())
		{
			/*std::cout << "[CRITICAL ERROR]: Instruction out of bounds!" << std::endl;
			break;*/
			goto DONE;
			return 0;
		}

		switch (instructs[instPtr])
		{
		case '+':
		{
			++MEM[curPtr];
			if (MEM[curPtr] > 255)
				MEM[curPtr] -= 256;
			break;
		}
		case '-':
		{
			--MEM[curPtr];
			if (MEM[curPtr] < 0)
				MEM[curPtr] += 256;
			break;
		}
		case '>':
		{
			if (curPtr < MEM_SIZE)
				++curPtr;
			else
				curPtr = 0;
			break;
		}
		case '<':
		{
			if (curPtr > 0)
				--curPtr;
			else
				curPtr = MEM_SIZE - 1;
			break;
		}
		case ',':
		{
			std::cin >> MEM[curPtr];
			break;
		}
		case '.':
		{
			std::cout << (char)MEM[curPtr];
			break;
		}
		case '[':
		{
			if (MEM[curPtr] == 0)
			{
				//SKIP TO MATCHING ']'
				instPtr = findMatchingBrace(instructs, true, instPtr);
			}
			break;
		}
		case ']':
		{
			//ALWAYS RETURN TO MATCHING '['
			instPtr = findMatchingBrace(instructs, false, instPtr);
			//--instPtr;
			//print_mem();
			break;
		}
		case '!':
		{
			goto DONE;
			return 0;
		}
		default:
		{
			std::cout << "[CRITICAL ERROR]: Invalid instruction: " << instructs[instPtr] << std::endl;
		}
		}
		++instPtr;
	}

	DONE:
	std::cout << std::endl << std::endl << "*** Reached end of program! ***" << std::endl;
	getchar();
	getchar();

    return 0;
}

